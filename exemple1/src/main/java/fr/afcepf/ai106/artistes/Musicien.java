package fr.afcepf.ai106.artistes;

import fr.afcepf.ai106.musique.Instrument;

public class Musicien implements Performeur {

	private String morceau;
	private Instrument instrument;
	
	public void perform() {
		
		System.out.println("Je joue " + morceau);
		instrument.jouer();

	}

	public Musicien() {
		System.out.println("Je cr�e un musicien");
	}
	
	@Override
	public String toString() {
		return "Musicien [morceau=" + morceau + ", instrument=" + instrument + ", getMorceau()=" + getMorceau()
				+ ", getInstrument()=" + getInstrument() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	public String getMorceau() {
		return morceau;
	}

	public void setMorceau(String morceau) {
		this.morceau = morceau;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	
}
